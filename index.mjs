import utils from 'util';
import * as utils_one from 'test-utils';

const params = { password: 'sdf234fdsf32cdsc' };

const myFunctionPromise = utils.promisify(utils_one.runMePlease);

try {
    const result = await myFunctionPromise(params);
    console.log(result);
} catch (error) {
    console.log(error);
}